import React, { Component } from 'react';
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            name:this.props.name || '',
            age:'',
            animal:'',
            submitted:false
         }
    
        this.setName = this.setName.bind(this);
        this.setAge = this.setAge.bind(this);
        this.setAnimal = this.setAnimal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
   
      }

    handleSubmit(e){
    e.preventDefault();
    console.log(e);
      this.setState({submitted: true})
      
    }
    setName(e){
     this.setState({name: e.target.value})
    }
    setAge(e){
        this.setState({age: e.target.value})
    }
    setAnimal(e){
    this.setState({animal: e.target.value})
    }
    render() { 
        if(!this.state.submitted){
            return ( 
                <div className="Container">
                
                    <h1 data-testid="h1tag">
                        Welcome! {this.state.name}
                    </h1>
      
                    <form data-testid="form" onSubmit={this.handleSubmit}>
                        
                        <label htmlFor = 'name'>Enter your name:</label>
                        <input type="text" id = 'name' name="name" value={this.state.value} onChange={this.setName}/>
                        
                        <label htmlFor = 'age'>Enter your age:</label>
                        <input id = 'age' type="text" name="name" value={this.state.value} onChange={this.setAge}/>
                        
                        <label htmlFor= 'animal'>Enter your favorite animal:</label>
                        <input  id = 'animal' type="text" name="name" value={this.state.value} onChange={this.setAnimal}/>
                        
                        <input type="submit" value="Submit" className="button"  />
                    </form>

                </div>
            );
        }
        else{
          
            return ( 
                <div className="Container">
                
                    <h1>
                        Welcome! {this.state.name}
                    </h1>
                    <h2>
                        You are {this.state.age} years old.
                    </h2>
                    <h2>
                        Your favorite animal is {this.state.animal}.
                    </h2>
                    <a
                        className="App-link"
                        href={"https://www.google.com/search?q="+this.state.animal}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                     Find my animal
                    </a>
                </div>
            );
        }
    }
}
 
export default Home;